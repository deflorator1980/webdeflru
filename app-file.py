from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    with open('debt.txt', 'r') as f2: 
        data = f2.read()

# It doesn't work
#    print(request.environ.get('HTTP_X_FORWARDED_FOR')) 
#    print(request.environ['HTTP_X_FORWARDED_FOR'])

    return data + '<br><br>' + request.headers.get('User-Agent') \
              + '<br><br>' +  request.remote_addr

# app.run(host='0.0.0.0', port=8081, debug=True) 
app.run(host='::', port=8081, debug=True) 
