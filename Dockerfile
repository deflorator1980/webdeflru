FROM ubuntu:20.04
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && apt-get install -y nginx vim micro curl \
    net-tools python3-pip wget iputils-ping

# vim-nginx plugin
RUN wget -O nginx.vim http://www.vim.org/scripts/download_script.php\?src_id\=19394
RUN mkdir -p ~/.vim/syntax
RUN mv nginx.vim ~/.vim/syntax/
RUN echo "au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif" > ~/.vim/filetype.vim
RUN touch ~/.vimrc
RUN echo "set nu" > ~/.vimrc

RUN rm /etc/nginx/sites-available/default 
RUN rm /etc/nginx/sites-enabled/default 
COPY nginx.conf /etc/nginx/
RUN mkdir /app
COPY fullchain.cer /app
COPY webdefl.ru.key /app
COPY app-file.py /app
COPY debt.txt /app
WORKDIR /app
RUN pip3 install --no-cache-dir flask
